<?php

namespace Drupal\netkata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create form to save database information.
 */
class NetkataConfiguration extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'netkata_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['netkata.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Getting the configuration value.
    $default_value = $this->config('netkata.settings');

    $form['netkata_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['netkata_config']['netkata_driver'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Select Driver'),
      '#options' => $this->getDriverOptions(),
      '#default_value' => $default_value->get('netkata_driver'),
    ];
    $form['netkata_config']['netkata_host'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $default_value->get('netkata_host'),
      '#required' => TRUE,
      '#title' => $this->t('Hostname'),
    ];
    $form['netkata_config']['netkata_database'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $default_value->get('netkata_database'),
      '#required' => TRUE,
      '#title' => $this->t('Database Name'),
    ];
    $form['netkata_config']['netkata_username'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $default_value->get('netkata_username'),
      '#required' => TRUE,
      '#title' => $this->t('Username'),
    ];
    $form['netkata_config']['netkata_password'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $default_value->get('netkata_password'),
      '#required' => FALSE,
      '#title' => $this->t('Password'),
    ];
    $form['netkata_config']['netkata_port'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $default_value->get('netkata_port') ? $default_value->get('netkata_port') : '3306' ,
      '#required' => TRUE,
      '#title' => $this->t('Port'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('netkata.settings');
    $netkata_host = $form_state->getValue('netkata_host');
    $netkata_username = $form_state->getValue('netkata_username');
    $netkata_password = $form_state->getValue('netkata_password');
    $netkata_port = $form_state->getValue('netkata_port');
    $netkata_database = $form_state->getValue('netkata_database');
    $netkata_driver = $form_state->getValue('netkata_driver');

    $config->set('netkata_host', $netkata_host)
      ->set('netkata_username', $netkata_username)
      ->set('netkata_password', $netkata_password)
      ->set('netkata_port', $netkata_port)
      ->set('netkata_database', $netkata_database)
      ->set('netkata_driver', $netkata_driver)
      ->save();
    drupal_set_message($this->t('Configuration has been saved.'));
    parent::submitForm($form, $form_state);
  }

  /**
   * Get Driver options.
   */
  protected function getDriverOptions() {
    return [
      '' => $this->t('-Select-'),
      'mysql' => $this->t('MySQL, MariaDB'),
    ];
  }

}
